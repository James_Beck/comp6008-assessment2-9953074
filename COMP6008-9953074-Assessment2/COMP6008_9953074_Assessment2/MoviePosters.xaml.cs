﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace COMP6008_9953074_Assessment2
{
    public partial class MoviePosters : ContentPage
    {
        public MoviePosters()
        {
            InitializeComponent();
            
            Img1.Source = MovieInfo.daDetails()[0].poster;
            Img2.Source = MovieInfo.daDetails()[1].poster;
            Img3.Source = MovieInfo.daDetails()[2].poster;
            Img4.Source = MovieInfo.daDetails()[3].poster;
            Img5.Source = MovieInfo.daDetails()[4].poster;
            Img6.Source = MovieInfo.daDetails()[5].poster;
            Img7.Source = MovieInfo.daDetails()[6].poster;
            Img8.Source = MovieInfo.daDetails()[7].poster;
            Img9.Source = MovieInfo.daDetails()[8].poster;
            Img10.Source = MovieInfo.daDetails()[9].poster;
            Img11.Source = MovieInfo.daDetails()[10].poster;
            Img12.Source = MovieInfo.daDetails()[11].poster;
            Img13.Source = MovieInfo.daDetails()[12].poster;
            Img14.Source = MovieInfo.daDetails()[13].poster;
            Img15.Source = MovieInfo.daDetails()[14].poster;
            Img16.Source = MovieInfo.daDetails()[15].poster;
            Img17.Source = MovieInfo.daDetails()[16].poster;
            Img18.Source = MovieInfo.daDetails()[17].poster;
            Img19.Source = MovieInfo.daDetails()[18].poster;
            Img20.Source = MovieInfo.daDetails()[19].poster;

            var tap = new TapGestureRecognizer();
            tap.Tapped += Tapthat;

            Img1.GestureRecognizers.Add(tap);
            Img2.GestureRecognizers.Add(tap);
            Img3.GestureRecognizers.Add(tap);
            Img4.GestureRecognizers.Add(tap);
            Img5.GestureRecognizers.Add(tap);
            Img6.GestureRecognizers.Add(tap);
            Img7.GestureRecognizers.Add(tap);
            Img8.GestureRecognizers.Add(tap);
            Img9.GestureRecognizers.Add(tap);
            Img10.GestureRecognizers.Add(tap);
            Img11.GestureRecognizers.Add(tap);
            Img12.GestureRecognizers.Add(tap);
            Img13.GestureRecognizers.Add(tap);
            Img14.GestureRecognizers.Add(tap);
            Img15.GestureRecognizers.Add(tap);
            Img16.GestureRecognizers.Add(tap);
            Img17.GestureRecognizers.Add(tap);
            Img18.GestureRecognizers.Add(tap);
            Img19.GestureRecognizers.Add(tap);
            Img20.GestureRecognizers.Add(tap);
        }

        private void Tapthat(object sender, EventArgs e)
        {
            int gridrow = (int)((BindableObject)sender).GetValue(Grid.RowProperty) * 4;
            int gridcolumn = (int)((BindableObject)sender).GetValue(Grid.ColumnProperty);
            int i = gridrow + gridcolumn;

            Navigation.PushAsync(new MovieDetails(MovieInfo.daDetails()[i].poster, MovieInfo.daDetails()[i].year.ToString(), MovieInfo.daDetails()[i].rating, MovieInfo.daDetails()[i].description));
        }
    }
}
