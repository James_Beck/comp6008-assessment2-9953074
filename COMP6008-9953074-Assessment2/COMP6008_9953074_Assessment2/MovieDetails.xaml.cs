﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace COMP6008_9953074_Assessment2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MovieDetails : ContentPage
    {
        public MovieDetails(string a, string b, string c, string d)
        {
            InitializeComponent();

            PosterImageLarge.Source = a;
            Date.Text = b;
            Rating.Text = c;
            Description.Text = d;
        }
    }
}
