﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMP6008_9953074_Assessment2
{
    class MovieInfo
    {
        public static List<ModelClass> daDetails()
        {
            List<ModelClass> details = new List<ModelClass>
            {
                new ModelClass {poster = "https://upload.wikimedia.org/wikipedia/en/4/43/Dr._No_-_UK_cinema_poster.jpg", year = 1962, rating = "7.3/10", description = "A resourceful British government agent seeks answers in a case involving the disappearance of a colleague and the disruption of the American space program. (110 mins.)"},
                new ModelClass {poster = "http://vignette2.wikia.nocookie.net/jamesbond/images/c/c5/From_Russia_With_Love_Poster.jpg/revision/latest?cb=20120720145849", year = 1963, rating = "7.5/10", description = "James Bond willingly falls into an assassination ploy involving a naive Russian beauty in order to retrieve a Soviet encryption device that was stolen by SPECTRE. (115 mins.)" },
                new ModelClass {poster = "http://static.rogerebert.com/uploads/movie/movie_poster/goldfinger-1964/large_34qwOWwSYMIXZXRvbWRHnEolxE1.jpg", year = 1964, rating = "7.8/10", description = "Investigating a gold magnate's smuggling, James Bond uncovers a plot to contaminate the Fort Knox gold reserve. (110 mins.)"},
                new ModelClass {poster = "https://s-media-cache-ak0.pinimg.com/originals/50/c0/1e/50c01eeb8dd2fba221b416d056676184.jpg", year = 1965, rating = "7.0/10", description = "James Bond heads to The Bahamas to recover two nuclear warheads stolen by SPECTRE agent Emilio Largo in an international extortion scheme. (130 mins.)"},
                new ModelClass {poster = "https://007fanart.files.wordpress.com/2010/06/yolt.jpg", year = 1967, rating = "6.9/10", description = "Agent 007 and the Japanese secret service ninja force must find and stop the true culprit of a series of spacejackings before nuclear war is provoked. (117 mins.)"},
                new ModelClass {poster = "http://www.antifonies.gr/wp-content/uploads/2016/01/on-her-majesty-s-secret-service-original.jpg", year = 1969, rating = "6.8/10", description = "James Bond woos a mob boss's daughter and goes undercover to uncover the true reason for Blofeld's allergy research in the Swiss Alps that involves beautiful women from around the world. (142 mins.)"},
                new ModelClass {poster = "https://007fanart.files.wordpress.com/2010/06/daf.jpg", year = 1971, rating = "6.7/10", description = "A diamond smuggling investigation leads James Bond to Las Vegas, where he uncovers an evil plot involving a rich business tycoon. (120 mins.)"},
                new ModelClass {poster = "http://www.impdb.org/images/3/32/Live_and_Let_Die_DVD_cover.jpg", year = 1973, rating = "6.8/10", description = "007 is sent to stop a diabolically brilliant heroin magnate armed with a complex organization and a reliable psychic tarot card reader. (121 mins.)"},
                new ModelClass {poster = "http://www.the007dossier.com/007dossier/james-bond-007-movie-posters/the-man-with-the-golden-gun/The-Man-With-T%20he-Golden-Gun-Poster-07.jpg", year = 1974, rating = "6.8/10", description = "James Bond is led to believe that he is targeted by the world's most expensive assassin while he attempts to recover sensitive solar cell technology that is being sold to the highest bidder. (125 mins.)"},
                new ModelClass {poster = "http://www.barbara-bach.com/tswlm1.jpg", year = 1977, rating = "7.1/10", description = "James Bond investigates the hijacking of British and Russian submarines carrying nuclear warheads with the help of a KGB agent whose lover he killed. (125 mins.)"},
                new ModelClass {poster = "http://static.rogerebert.com/uploads/movie/movie_poster/moonraker-1979/large_t7DrcBlDfhXUlKM7CQjM9Po9P4U.jpg", year = 1979, rating = "6.3/10", description = "James Bond investigates the mid - air theft of a space shuttle and discovers a plot to commit global genocide. (126 mins.)"},
                new ModelClass {poster = "https://007fanart.files.wordpress.com/2010/06/fyeo.jpg", year = 1981, rating = "6.8/10", description = "Agent 007 is assigned to hunt for a lost British encryption device and prevent it from falling into enemy hands. (127 mins.)"},
                new ModelClass {poster = "https://images-na.ssl-images-amazon.com/images/M/MV5BMjI2MDE0NjE1NV5BMl5BanBnXkFtZTcwNjYyMzY0NA@@._V1.._UY1200_CR85,0,630,1200_AL_.jpg", year = 1983, rating = "6.6/10", description = "A fake Fabergé egg and a fellow agent's death lead James Bond to uncover an international jewel-smuggling operation, headed by the mysterious Octopussy, being used to disguise a nuclear attack on N.A.T.O. forces. (131 mins.)"},
                new ModelClass {poster = "http://vignette1.wikia.nocookie.net/jamesbond/images/b/b4/Never_Say_Never_Again.png/revision/latest?cb=20120803162049", year = 1983, rating = "6.2/10", description = "A SPECTRE agent has stolen two American nuclear warheads, and James Bond must find their targets before they are detonated. (134 mins.)"},
                new ModelClass {poster = "http://static.tvtropes.org/pmwiki/pub/images/view_to_a_kill_ver3.jpg", year = 1985, rating = "6.3/10", description = "An investigation of a horse - racing scam leads 007 to a mad industrialist who plans to create a worldwide microchip monopoly by destroying California's Silicon Valley. (131 mins.)"},
                new ModelClass {poster = "http://static.rogerebert.com/uploads/movie/movie_poster/the-living-daylights-1987/large_cBk0TKOc64iR5TDi1k5bWAZwbhz.jpg", year = 1987, rating = "6.7/10", description = "James Bond is living on the edge to stop an evil arms dealer from starting another world war.Bond crosses all seven continents in order to stop the evil Whitaker and General Koskov. (130 mins.)"},
                new ModelClass {poster = "http://static.rogerebert.com/uploads/movie/movie_poster/licence-to-kill-1989/large_4lpfRGW7dCqlco2n6wqptyFAz4V.jpg", year = 1989, rating = "6.6/10", description = "James Bond goes rogue and sets off to unleash vengeance on a drug lord who tortured his best friend, a C.I.A.agent, and left him for dead and murdered his bride after he helped capture him. (133 mins.)"},
                new ModelClass {poster = "http://fontmeme.com/images/007-golden-eye-Poster.jpg", year = 1995, rating = "7.2/10", description = "James Bond teams up with the lone survivor of a destroyed Russian research center to stop the hijacking of a nuclear space weapon by a fellow agent formerly believed to be dead. (130 mins.)"},
                new ModelClass {poster = "http://www.007museum.com/Tomorrow_Never_Dies_poster_BMW.jpg", year = 1997, rating = "6.5/10", description = "James Bond heads to stop a media mogul's plan to induce war between China and the UK in order to obtain exclusive global media coverage. (119 mins.)"},
                new ModelClass {poster = "http://static.rogerebert.com/uploads/movie/movie_poster/the-world-is-not-enough-1999/large_tCarTEKvXjALk87r3wAxB4jb1ON.jpg", year = 1999, rating = "6.4/10", description = "James Bond uncovers a nuclear plot when he protects an oil heiress from her former kidnapper, an international terrorist who can't feel pain. (128 mins.)"},
            };

            return details;
        }
    }
}
