﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMP6008_9953074_Assessment2
{
    public class ModelClass
    {
        public string poster { get; set; }
        public int year { get; set; }
        public string rating { get; set; }
        public string description { get; set; }
    }
}
